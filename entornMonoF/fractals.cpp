//******** PRACTICA VISUALITZACI� GR�FICA INTERACTIVA
//******** Marc Vivet, Carme Juli�, D�bora Gil, Enric Mart� (Setembre 2012)
// fractals.cpp : Funcions de lectura i generaci� de fractals 


#include "stdafx.h"
#include "fractals.h"
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include "const_fractal.h"
#include <fstream>
#include <string>
#include <iostream>
#include "escena.h"
#include "normals.h"

using namespace std;

#include <time.h>

// G2: Variables utilitzades en els fractals
// FMAX: Index m�xim de la matriu d'al�ades. Definida a constants.h (valor:512)
double zz[FMAX+1][FMAX+1];
double normalsC[FMAX+1][FMAX+1][3];
double normalsV[FMAX+1][FMAX+1][3];

// Valors m�xim i m�nim de Z necessaris per interpolar
// correctament la paleta de colors (iluminaci� suau del fractal)
double zmax=0; 
double zmin=0;

// Variables pics
double cx[6],cy[6],radi[6],hmax[6]; // Centres, radis i al�ades de les muntanyes
double a=1.0*FMAX*(0.65);			// Parametre lemniscata

// N�mero de pics
int npics=0;

double r;

// llegir_pts: Funci� de lectura d'un fitxer fractal amb pics.
// Variables:	- nomf: Nom del fitxer a llegir.
int llegir_pts(CString nomf)
{	int i,j,step;
	FILE *fd;
// 1. INICIALITZAR MATRIUS zz i normals
for(i=0;i<=512;i=i++)
	{ for(j=0;j<=FMAX;j=j++)
		{	zz[i][j]=0.0;
			normalsC[i][j][0]=0.0;
			normalsC[i][j][1]=0.0;
			normalsC[i][j][2]=0.0;
			normalsV[i][j][0]=0.0;
			normalsV[i][j][1]=0.0;
			normalsV[i][j][2]=0.0;
		}
	}

// 2. INICIALITZAR PICS
for(i=0;i<=6;i=i++)
	{ cx[i]=0; cy[i]=0; radi[i]=0; hmax[i]=0;
	}

// 3. OBRIR FITXER FRACTAL I LLEGIR AL�ADES ASSIGNANT-LES
//    A LA MATRIU ZZ DE FORMA EQUIESPAIADA.
char cadena[128];
ifstream fe(nomf);
fe.getline(cadena,128);
char * pch;
pch = strtok (cadena," ");
int var[2];
int cont = 0;
while (pch!= NULL)
{
	var[cont] = atoi(pch);
	pch = strtok (NULL, " ");
	cont++;
}

int max_i = var[0];
int max_j = var[1];

int pas = 1;

if (max_i%2 == 0) pas = max_i;
else pas = max_i-1;

pas = 512/pas;

for (int i = 0; i <=FMAX; i++)
{
	for(int j = 0; j <=FMAX;j++) zz[i][j] = 0.0;
}

for (int i = 0; i <=FMAX; i+=pas)
{
	for(int j = 0; j<=FMAX;j+=pas)
	{
		fe.getline(cadena,128);
		zz[i][j] = atof(cadena);
	}
}

// 4. LLEGIR EL NOMBRE DE PICS I ELS VALORS (CENTRE,RADI 
//    I AL�ADA M�XIMA.

fe.getline(cadena,128);
int max = atoi(cadena);

for (int i = 0; i< max; i++)
{
	fe.getline(cadena,128);
	pch = strtok (cadena," ");
	int cont = 0;
	while (pch!= NULL)
	{
		switch(cont)
		{
		case 0:
			cx[i] = atof(pch);
			break;
		case 1:
			cy[i] = atof(pch);
			break;
		case 2:
			radi[i] = atof(pch);
			break;
		case 3:
			hmax[i] = atof(pch);
			break;
		}
		pch = strtok (NULL, " ");
		cont++;
	}
}

// 5. INICIALITZAR LA VARIABLE ALEAT�RIA
srand( (unsigned) time(NULL));
r=(double) rand()/RAND_MAX;

// 6. C�LCUL DEL M�XIM I M�NIM DE LES AL�ADES INICIALS

// Funci� retorna el pas entre al�ades a la variable step, 
// calculat en funci� del nombre d'al�ades inicials i del
// tamany de la matriu.
return pas;

}

// escriure_pts: Funci� d'escriptura en un fitxer de les al�ades i pics 
//               d'un fractal segons la resoluci� actual.
// Variables:	- nomf: Nom del fitxer a escriure.
//				- paso: Resoluci� del fractal a escriure.
bool escriure_pts(CString nomf,int paso)
{
	int contador_aux_2 = 0;

	int contador_aux = 0;

	for (int i = 0; i <= FMAX; i+=paso) contador_aux++;

	ofstream myfile;

	myfile.open (nomf+".MNT");
	myfile << contador_aux << " " << contador_aux << endl;
	for (int i = 0; i <= FMAX; i+=paso)
	{
		for (int j = 0; j <= FMAX; j+=paso)
		{
			myfile << zz[i][j] << endl;
		}
	}
	for (int i = 0; i <6; i++)
	{
		if(hmax[i] != 0.0) contador_aux_2++; 
	}
	myfile << contador_aux_2 << endl;
	for (int i = 0; i< contador_aux_2;i++)
	{
		myfile << cx[i] << " " << cy[i] << " " << radi[i] << " " << hmax[i] << endl;
	}
	myfile.close();

	return false;

}

// itera_fractal: C�lcul de les al�ades intermitges.
// Variables: - bruit: Tipus de soroll (Linial,Quadr�tic,SQRT o diferenciable)
//            - paso: Pas d'iteraci� del fractal.
void itera_fractal(char bruit,int paso)
{
	for (int i = 0; i <= FMAX; i+=paso)
	{
		for(int j = 0; j <= FMAX; j+=paso)
		{
			if (j != FMAX && i != FMAX)
			{
				zz[(i+i+paso+i+i+paso)/4][(j+j+j+paso+j+paso)/4]= ((zz[i][j]+zz[i][j+paso]+zz[i+paso][j]+zz[i+paso][j+paso])/4)+soroll((i+i+paso+i+i+paso)/4,(j+j+j+paso+j+paso)/4,paso,bruit);
				zz[(i+i)/2][(j+j+paso)/2] = ((zz[i][j]+zz[i][j+paso])/2)+soroll((i+i)/2,(j+j+paso)/2,paso,bruit);
				zz[(i+paso+i+paso)/2][(j+j+paso)/2] = ((zz[i+paso][j]+zz[i+paso][j+paso])/2)+soroll((i+paso+i+paso)/2,(j+j+paso)/2,paso,bruit);
				zz[(i+i+paso)/2][(j+paso+j+paso)/2] = ((zz[i][j+paso]+zz[i+paso][j+paso])/2)+soroll((i+i+paso)/2,(j+paso+j+paso)/2,paso,bruit);
				zz[(i+i+paso)/2][(j+j)/2] = ((zz[i][j]+zz[i+paso][j])/2)+soroll((i+i+paso)/2,(j+j)/2,paso,bruit);
			}
		}
	}
}


//soroll: Calcul del soroll en les alsades segons la dist�ncia
//        als picsdistancia.
// Variables: - i,j: Posici� de l'al�ada a calcular el soroll..
// 			  - alf: M�xim valor perm�s. En el nostre cas, el valor del
//                   de la variable pas que ens d�na la resoluci� del fractal.
//            - noise: Tipus de soroll (linial, quadr�tic,sqrt o diferenciable).
double soroll(int i,int j,double alf,char noise)
{
	double ff,s,r;

// C�lcul de la variable aleat�ria entre (0,1).
	r=(double) rand()/RAND_MAX;

// C�lcul del soroll segons el tipus.
	switch(noise)
	{
	case S_LINIAL:
		ff = soroll_lin(i,j);
		break;
	case S_QUADRATIC:
		ff=soroll_quad(i,j);
		break;
	case S_SQRT:
		ff=soroll_sq(i,j);
		break;
	case S_DIFERENCIABLE:
		ff=soroll_dif(i,j);
		break;
	default:
		ff=0;
	} 
	s=ff*r*alf;
//Retorn del valor del soroll.
	return s;
	
}

int comprovar_color(int max, int zz,int pas)
{	
	double umbral = max/23;

	for (int k=0; k < 23;k++)
	{
		if(zz <= (umbral*(k+1)))
		{
			return k;
		}
	}
}
// TRIANGULACIO DEL TERRENY. Dibuix de la muntanya fractal
// Variables: - Iluminaci�: Defineix el tipus d'iluminaci� (filferros, plana o suau).
//            - step: Pas de dibuix del fractal.
void fract(char iluminacio,int step)
{
	int x;
	switch(iluminacio)
	{
	case FILFERROS:
		glPushMatrix();
			x = 0;
			for (int i = -256; i < 256; i+=step)
			{
				int y = 0;
				for (int j = -256; j < 256; j+=step)
				{
					
					glBegin(GL_TRIANGLES);
						glVertex3f(i,j,zz[x][y]); // V1
						glVertex3f(i+step,j,zz[x+step][y]); // V2
						glVertex3f(i+step,j+step,zz[x+step][y+step]); // V3
					glEnd();
					glBegin(GL_TRIANGLES);
						glVertex3f(i,j,zz[x][y]); // V1
						glVertex3f(i+step,j+step,zz[x+step][y+step]); // V3
						glVertex3f(i,j+step,zz[x][y+step]); // V4
					glEnd();
					y += step;
				}
				x += step;
			}
	
		glPopMatrix();
		break;
	case PLANA:
		normalcara(step);
		glPushMatrix();
		x = 0;
		for (int i = -256; i < 256; i+= step)
		{
			int y = 0;
			for (int j = -256; j < 256; j+=step)
			{
				glBegin(GL_TRIANGLES);
					glNormal3f(normalsC[x][y][0], normalsC[x][y][1], normalsC[x][y][2]); // Vector Normal
					glVertex3f(i,j,zz[x][y]); // V1
					glVertex3f(i+step,j,zz[x+step][y]); // V2
					glVertex3f(i+step,j+step,zz[x+step][y+step]); // V3
				glEnd();
				glBegin(GL_TRIANGLES);
					glNormal3f(normalsV[x][y][0], normalsV[x][y][1], normalsV[x][y][2]); // Vector Normal
					glVertex3f(i,j,zz[x][y]); // V1
					glVertex3f(i+step,j+step,zz[x+step][y+step]); // V3
					glVertex3f(i,j+step,zz[x][y+step]); // V4
				glEnd();
				y += step;
			}
			x += step;
		}
		glPopMatrix();
		break;
	case SUAU:
		double b = 0;
		for (int i = 0; i<= FMAX; i+=step)
		{
			for(int j = 0; j <= FMAX; j+=step)
			{
				if(b < zz[i][j]) b = zz[i][j];
			}
		}
		normalvertex(step);
		glPushMatrix();
		x = 0;
		for (int i = -256; i < 256; i+= step)
		{
			int y = 0;
			int color;
			for (int j = -256; j < 256; j+=step)
			{
				glBegin(GL_TRIANGLES);
					glNormal3f (normalsV[x][y][0],normalsV[x][y][1],normalsV[x][y][2]); // Normal a V1
					color = comprovar_color(b,zz[x][y],step);
					glColor3f(colorR[color],colorG[color],colorB[color]);
					glVertex3f(i,j,zz[x][y]); // V1
					glNormal3f(normalsV[x+step][y][0],normalsV[x+step][y][1],
					normalsV[x+step][y][2]); // Normal a V2
					color = comprovar_color(b,zz[x+step][y],step);
					glColor3f(colorR[color],colorG[color],colorB[color]);
					glVertex3f(i+step,j,zz[x+step][y]); // V2
					glNormal3f(normalsV[x+step][y+step][0],normalsV[x+step][y+step][1],
					normalsV[x+step][y+step][2]); // Normal a V3
					color = comprovar_color(b,zz[x+step][y+step],step);
					glColor3f(colorR[color],colorG[color],colorB[color]);
					glVertex3f(i+step,j+step,zz[x+step][y+step]); // V3
				glEnd();
				glBegin(GL_TRIANGLES);
					glNormal3f (normalsV[x][y][0],normalsV[x][y][1],normalsV[x][y][2]); // Normal a V1
					color = comprovar_color(b,zz[x][y],step);
					glColor3f(colorR[color],colorG[color],colorB[color]);
					glVertex3f(i,j,zz[x][y]); // V1
					glNormal3f(normalsV[x+step][y+step][0],normalsV[x+step][y+step][1],
					normalsV[x+step][y+step][2]); // Normal a V3
					color = comprovar_color(b,zz[x+step][y+step],step);
					glColor3f(colorR[color],colorG[color],colorB[color]);
					glVertex3f(i+step,j+step,zz[x+step][y+step]); // V3
					glNormal3f(normalsV[x][y+step][0],normalsV[x][y+step][1],
					normalsV[x][y+step][2]); // Normal a V4
					color = comprovar_color(b,zz[x][y+step],step);
					glColor3f(colorR[color],colorG[color],colorB[color]);
					glVertex3f(i,j+step,zz[x][y+step]); // V4
				glEnd();
				y += step;
			}
			x += step;
		}
		glPopMatrix();
		break;
	}
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glColor4f(0.2f,0.75f,0.9f,0.5f);
	glRectf(-256,-256,256,256);
	glDisable(GL_BLEND);
	
}

//------------------ CALCUL DELS SOROLLS  --------------------/

// C�lcul del soroll linial segons la posici� del punt (x,y)
double soroll_lin(int x,int y)
{ 
	double total = 0;
	for (int i = 0; i < 6; i++)
	{
		if (cx[i] != 0.0 && cy[i] != 0)
		{
			double p1 = (cx[i] - x)*(cx[i]-x);
			double p2 = (cy[i] - y)*(cy[i] - y);
			double distancia = sqrt(p1+p2);
			if (distancia < radi[i])
			{
				total += hmax[i]*(1-(distancia/radi[i]));
			}
		}
	}
	return total;
}

// C�lcul del soroll quadr�tic segons la posici� del punt (x,y)
double soroll_quad(int x,int y)
{ 
	double total = 0;
	for (int i = 0; i < 6; i++)
	{
		if (cx[i] != 0.0 && cy[i] != 0)
		{
			double p1 = (cx[i] - x)*(cx[i]-x);
			double p2 = (cy[i] - y)*(cy[i] - y);
			double distancia = sqrt(p1+p2);
			if (distancia < radi[i])
			{
				total += hmax[i]*(1-(distancia*distancia)/(radi[i]*radi[i]));
			}
		}
	}
	return total;
}

// C�lcul del soroll arrel quadrada segons la posici� del punt (x,y)
double soroll_sq(int x,int y)
{ 
	double total = 0;
	for (int i = 0; i < 6; i++)
	{
		if (cx[i] != 0.0 && cy[i] != 0)
		{
			double p1 = (cx[i] - x)*(cx[i]-x);
			double p2 = (cy[i] - y)*(cy[i] - y);
			double distancia = sqrt(p1+p2);
			if (distancia < radi[i])
			{
				total += hmax[i]*(1-(sqrt(distancia))/(sqrt(radi[i])));
			}
		}
	}
	return total;
}

// C�lcul del soroll diferenciable segons la posici� del punt (x,y)
double soroll_dif(int x,int y)
{ 
	double total = 0;
	for (int i = 0; i < 6; i++)
	{
		if (cx[i] != 0.0 && cy[i] != 0)
		{
			double p1 = (cx[i] - x)*(cx[i]-x);
			double p2 = (cy[i] - y)*(cy[i] - y);
			double distancia = sqrt(p1+p2);
			if (distancia < radi[i])
			{
				total += hmax[i]*(1-(distancia/radi[i]))*(1-(distancia/radi[i]));
			}
		}
	}
	return total;
}
