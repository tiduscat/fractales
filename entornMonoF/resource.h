//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Practica.rc
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDR_PRACTITYPE                  129
#define ID_CUB                          32773
#define ID_TETERA                       32774
#define ID_CON                          32776
#define ID_PERSPECTIVA                  32778
#define ID_FIXE                         32779
#define ID_MOBIL                        32780
#define ID_ZOOM                         32781
#define ID_ESFERA                       32783
#define ID_TEAPOT                       32785
#define ID_EIXOS                        32790
#define ID_FILFERROS                    32791
#define ID_PLANA                        32792
#define ID_SUAU                         32793
#define ID_I_FIXE                       32807
#define ID_PAN                          32811
#define ID_TEST                         32821
#define ID_TRASLACIO                    32822
#define ID_INITRAS                      32823
#define ID_ROTACIO                      32824
#define ID_INIROT                       32825
#define ID_ESCALATGE                    32826
#define ID_INIESCAL                     32827
#define ID_TRUCK                        32828
#define ID_ZBuffer                      32829
#define ID_BACK_LINE                    32830
#define ID_MOBILX                       32831
#define ID_MOBILY                       32832
#define ID_MOBILZ                       32833
#define ID_INIPAN                       32834
#define ID_NAVEGA                       32835
#define ID_ININAV                       32836
#define ID_OBJ3DS                       32837
#define ID_FULLSCREEN                   32838
#define ID_PROJECCI32839                32839
#define ID_ORTOGRAFICA                  32840
#define ID_VISTA_POLARSEIXZ             32841
#define ID_POLAR_Z                      32842
#define ID_VISTA_POLARSEIXY             32843
#define ID_POLAR_Y                      32844
#define ID_VISTA_POLARSEIXX             32845
#define ID_POLAR_X                      32846
#define ID_ARXIUS_OBRIRFITXER3DS        32847
#define ID_FILE_OPEN_3DS                32848
#define ID_ARXIUS_OBRIRFITXEROBJ        32849
#define ID_FILE_OPEN_OBJ                32850
#define ID_OBJECTE_FITXEROBJ            32851
#define ID_OBJOBJ                       32852
#define ID_PROJECCI32853                32853
#define ID_IL_TEXTURES                  32854
#define ID_TEXTURES                     32855
#define ID_I_TEXTURES                   32856
#define ID_IL_TEXTURA                   32857
#define ID_TEXTURA_FUSTA                32858
#define ID_TEXTURA_MARBRE               32859
#define ID_TEXTURA_MET                  32860
#define ID_TEXTURA_FITXERBMP            32861
#define ID_OBJECTE_CAMI32862            32862
#define ID_CAMIO                        32863
#define ID_ItMes                        32864
#define ID_ItMenys                      32865
#define ID_OBJECTE_ASDASD               32866
#define ID_OBJECTE_FRACTALS             32867
#define ID_OBJECTE_FRACTAL              32868
#define ID_FRACTAL_ESCENA               32869
#define ID_FRACTAL_SOROLL               32870
#define ID_ESCENA_MUNTANYA              32871
#define ID_ESCENA_RIU                   32872
#define ID_ESCENA_CIRCUIT               32873
#define ID_ESCENA_PAISSATGE             32874
#define ID_SOROLL_SENSE                 32875
#define ID_SOROLL_LINEAL                32876
#define ID_SOROLL_QUADR32877            32877
#define ID_SOROLL_DIFERENCIABLE         32878
#define ID_SOROLL_SQRT                  32879
#define ID_ESCENA_FITXER                32880
#define ID_Menu                         32881
#define ID_IL_SOL                       32882
#define ID_Menu32883                    32883
#define ID_Sol                          32884
#define ID_INDICATOR_R                  59142
#define ID_INDICATOR_ALFA               59143
#define ID_INDICATOR_BETA               59144
#define ID_INDICATOR_PVX                59145
#define ID_INDICATOR_PVY                59146
#define ID_INDICATOR_PVZ                59147
#define ID_INDICATOR_PAS                59148
#define ID_INDICATOR_TR                 59149
#define ID_INDICATOR_PTR                59150

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         32885
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
