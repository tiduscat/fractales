//******** PRACTICA VISUALITZACI� GR�FICA INTERACTIVA (Entorn Basic)
//******** Marc Vivet, Carme Juli�, D�bora Gil, Enric Mart� (Setembre 2012)
// fractals.h : interface de fractals.cpp
//

#ifndef FRACTALS
#define FRACTALS

// Lectura i escriptura fitxers de fractals.
int llegir_pts(CString nomf);
bool escriure_pts(CString nomf,int paso);

// C�lcul d'al�ades intermitges
void itera_fractal(char bruit,int paso);

// C�lcul del soroll per cada al�ada (i,j)
double soroll(int i,int j,double alf,char noise);

// Soroll linial segons la posici� del punt (x,y)
double soroll_lin(int x,int y);
// Soroll quadr�tic segons la posici� del punt (x,y)
double soroll_quad(int x,int y);
// Soroll arrel quadrada segons la posici� del punt (x,y)
double soroll_sq(int x,int y);
// Soroll diferenciable segons la posici� del punt (x,y)
double soroll_dif(int x,int y);

// Dibuix i triangulaci� del terreny
void fract(char iluminacio,int step);

#endif
